/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 8);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var canvas = exports.canvas = document.querySelector('canvas');
var ctx = exports.ctx = canvas.getContext('2d');
var _playerwidth = exports._playerwidth = 108;
var _playerheight = exports._playerheight = 140;
var player = exports.player = void 0;
var up = exports.up = void 0,
    left = exports.left = void 0,
    right = exports.right = false;
var jumping = exports.jumping = void 0,
    shooting = exports.shooting = true;
var friction = exports.friction = 1.02;
var gravity = exports.gravity = 2.8;
var obstacle = exports.obstacle = void 0;
var obstacle_array = exports.obstacle_array = [];
var nowstamp = exports.nowstamp = void 0,
    desiredstamp = exports.desiredstamp = void 0;
var bullet = exports.bullet = void 0;
var bullets = exports.bullets = [];
var imageObj = exports.imageObj = void 0,
    bigObj = exports.bigObj = void 0,
    smallObj = exports.smallObj = void 0;
var fireburst = exports.fireburst = void 0;
var deltastamp = exports.deltastamp = 2;
var player_score = exports.player_score = 0;
var current_score = exports.current_score = 0;
var playerframes = exports.playerframes = 7;
var currentframe = exports.currentframe = 0;
var playerframestamp = exports.playerframestamp = 70;
var playerframenow = exports.playerframenow = void 0,
    playerframedesired = exports.playerframedesired = void 0;
var gameanimation = exports.gameanimation = void 0;
var canvas_bg = exports.canvas_bg = void 0,
    ground_bg = exports.ground_bg = void 0;
var desiredHeight = exports.desiredHeight = void 0;
var desiredWidth = exports.desiredWidth = void 0;
var velocityfactor = exports.velocityfactor = 3;
var jumpingamount = exports.jumpingamount = 36;
var bgspeed = exports.bgspeed = 2;
var groundspeed = exports.groundspeed = 8;

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.randomX = randomX;
exports.update_player_score = update_player_score;
exports.clearCanvas = clearCanvas;
exports.keyListener = keyListener;
exports.removeObstacles = removeObstacles;

var _variables = __webpack_require__(0);

var variable = _interopRequireWildcard(_variables);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function randomX(between, to) {
  return Math.floor(Math.random() * (to - between + 1) + between);
}

function update_player_score() {
  variable.ctx.font = "30px Arial";
  variable.ctx.strokeStyle = 'black';
  variable.ctx.lineWidth = 4;
  variable.ctx.strokeText(variable.player_score, 10, 50);
  variable.ctx.fillStyle = "white";
  variable.ctx.fillText(variable.player_score, 10, 50);
}

function clearCanvas() {
  variable.ctx.clearRect(0, 0, variable.canvas.width, variable.canvas.height);
}

function keyListener(event) {

  var key_state = event.type == "keydown" ? true : false;

  switch (event.keyCode) {

    case 38:
      // up key
      variable.up = key_state;
      console.log(variable.up);
      break;
    case 32:
      // space bar
      variable.shooting = key_state;
      if (variable.shooting) {
        variable.fireburst = new Audio();
        if (variable.fireburst.canPlayType('audio/mp3')) {
          variable.fireburst.src = 'audio/laser.mp3';
        } else {
          variable.fireburst.src = 'audio/laser.wav';
        }
        variable.fireburst.play();
      }

  }
}

function removeObstacles() {
  var i = variable.obstacle_array.length;

  while (i--) {
    var obstacle = variable.obstacle_array[i];

    if (obstacle.hasUpdatedScore) {
      variable.obstacle_array.splice(i, 1);
    }
  }
}

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _variables = __webpack_require__(0);

var variable = _interopRequireWildcard(_variables);

var _utilities = __webpack_require__(1);

var utility = _interopRequireWildcard(_utilities);

var _obstacle = __webpack_require__(6);

var _player = __webpack_require__(7);

var _bullet = __webpack_require__(5);

var _background = __webpack_require__(4);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

var Game = {
  start: function start() {
    utility.clearCanvas();
    var splashScreen = document.querySelector('.splashscreen');
    var gameoverscreen = document.querySelector('.gameoverscreen');

    variable.canvas.classList.add('playing');
    splashScreen.classList.remove('open');
    splashScreen.classList.add('not-displayed');
    gameoverscreen.classList.remove('open');

    init();
    animate();
  },
  stop: function stop() {}
};

// NOT YET USED!!!!!!!!!!!!!!!!!!!!!!!!!
function returnCollision(x1, y1, w1, h1, x2, y2, w2, h2) {
  if (x1 + w1 >= x2 && x2 + w2 >= x && y1 + h1 >= y2 && y2 + h2 >= y1) {
    return true;
  } else {
    return false;
  }
}

/* ========================== */
var bg1, bg2, bg3, bg4;

function initBackground() {
  variable.canvas_bg = new Image();
  variable.ground_bg = new Image();

  variable.canvas_bg.onload = function () {
    variable.ctx.drawImage(variable.canvas_bg, 0, 0);
  };
  variable.ground_bg.onload = function () {
    variable.ctx.drawImage(variable.ground_bg, 0, 300);
  };

  variable.canvas_bg.src = 'images/forest.png';
  variable.ground_bg.src = 'images/ground.png';

  bg1 = new _background.Background(0, 0, variable.bgspeed, variable.canvas_bg);
  bg2 = new _background.Background(variable.canvas.width, 0, variable.bgspeed, variable.canvas_bg);

  bg3 = new _background.Background(0, 190, variable.groundspeed, variable.ground_bg);
  bg4 = new _background.Background(variable.canvas.width, 190, variable.groundspeed, variable.ground_bg);
}

function initObstacleImages() {
  variable.bigObj = new Image();
  variable.smallObj = new Image();

  variable.bigObj.src = 'images/spike-1.png';
  variable.smallObj.src = 'images/spike-2.png';
}

function initPlayer() {

  variable.imageObj = new Image();

  variable.imageObj.onload = function () {
    variable.ctx.drawImage(variable.imageObj, 0, 0);
  };
  variable.imageObj.src = 'images/player3.png';
}

function init() {
  resizeCanvas();

  initPlayer();
  initObstacleImages();
  initBackground();

  variable.player = new _player.Player(30, variable.canvas.height / 2, 0, 0, variable._playerwidth, variable._playerheight);

  //get current timestamp in seconds
  variable.nowstamp = Math.floor(Date.now() / 1000);
  variable.playerframenow = Math.floor(Date.now());
}

function resizeCanvas() {
  var canvasWidth = variable.canvas.width;
  var canvasHeight = variable.canvas.height;
  variable.desiredWidth = window.innerWidth;
  var ratio = canvasWidth / canvasHeight;

  variable.desiredHeight = variable.desiredWidth / ratio;

  variable.canvas.style.width = variable.desiredWidth + "px";
  variable.canvas.style.height = variable.desiredHeight + "px";

  initBackground();
}

function animate() {
  variable.gameanimation = requestAnimationFrame(animate);

  utility.clearCanvas();

  bg1.update();
  bg2.update();
  bg3.update();
  bg4.update();

  variable.playerframedesired = Math.floor(Date.now());

  if (!variable.jumping) {

    if (variable.currentframe >= variable.playerframes) {
      variable.currentframe = 0;
    } else {
      if (variable.playerframedesired - variable.playerframenow >= variable.playerframestamp) {
        variable.currentframe += 1;
        variable.playerframenow = Math.floor(Date.now());
      }
    }
  } else {
    variable.currentframe = 0;
  }

  variable.player.update(variable.obstacle_array);

  variable.desiredstamp = Math.floor(Date.now() / 1000);

  // when more seconds have passed than deltastamp, spawn a new obstacle and push it into the array of obstacles
  if (variable.desiredstamp - variable.nowstamp >= variable.deltastamp) {
    if (utility.randomX(1, 2) == 1) {
      variable.obstacle = new _obstacle.BigObstacle(utility.randomX(variable.canvas.width + 100, variable.canvas.width + 150), variable.canvas.height - 150, 4, 80, 76, false);
    } else {
      variable.obstacle = new _obstacle.SmallObstacle(utility.randomX(variable.canvas.width + 100, variable.canvas.width + 150), variable.canvas.height - 150, 4, 38, 76, false);
    }
    variable.obstacle_array.push(variable.obstacle);
    variable.nowstamp = Math.floor(Date.now() / 1000);
  }

  // render the obstacles
  for (var i = 0; i < variable.obstacle_array.length; i++) {
    variable.obstacle_array[i].update();
  }

  // checking if spacebar is hit, and create a bullet and store it.
  if (variable.shooting) {
    variable.bullet = new _bullet.Bullet(variable.player.x + variable._playerwidth, variable.player.y + variable._playerheight / 2, 10, 6, 2);
    variable.bullets.push(variable.bullet);
  }

  // iterate bullets and draw them
  for (var k = 0; k < variable.bullets.length; k++) {
    variable.bullets[k].update(variable.player);
  }

  // remove obstacles when they have updated score (and they are offscreen)
  utility.removeObstacles();
  utility.update_player_score();
}

/* ============= */
window.onload = function () {
  var startGame = document.querySelector('.startGame');
  var restartGame = document.querySelector('.restartgame');
  startGame.addEventListener('click', Game.start);
  //restartGame.addEventListener('click', Game.start);

  window.addEventListener('keydown', utility.keyListener);
  window.addEventListener('keyup', utility.keyListener);
  setInterval(function () {
    variable.velocityfactor += .3;
  }, 10000);
  //Game.start();
};

window.addEventListener('resize', resizeCanvas);

/***/ }),
/* 3 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Background = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _variables = __webpack_require__(0);

var variable = _interopRequireWildcard(_variables);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Background = exports.Background = function () {
  function Background(x, y, speed, imagetodraw) {
    _classCallCheck(this, Background);

    this.x = x;
    this.y = y;
    this.speed = speed;
    this.imagetodraw = imagetodraw;
  }

  _createClass(Background, [{
    key: "draw",
    value: function draw() {
      variable.ctx.beginPath();
      variable.ctx.drawImage(this.imagetodraw, this.x, this.y, variable.canvas.width, variable.canvas.height);
    }
  }, {
    key: "update",
    value: function update() {
      this.x -= this.speed;

      if (this.x + variable.canvas.width <= 0) {
        console.log('repeat bg');
        this.x = variable.canvas.width - 2;
      }

      this.draw();
    }
  }]);

  return Background;
}();

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Bullet = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _variables = __webpack_require__(0);

var variable = _interopRequireWildcard(_variables);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Bullet = exports.Bullet = function () {
  function Bullet(x, y, dx, bulletwidth, bulletheight) {
    _classCallCheck(this, Bullet);

    this.x = x;
    this.y = y;
    this.dx = dx;
    this.bulletwidth = bulletwidth;
    this.bulletheight = bulletheight;
  }

  _createClass(Bullet, [{
    key: "update",
    value: function update(player) {
      if (variable.shooting == true) {
        variable.shooting = false;
      }

      this.x += this.dx;

      this.draw();
    }
  }, {
    key: "draw",
    value: function draw() {
      variable.ctx.beginPath();
      variable.ctx.fillStyle = "red";
      variable.ctx.fillRect(this.x, this.y, this.bulletwidth, this.bulletheight);
    }
  }]);

  return Bullet;
}();

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SmallObstacle = exports.BigObstacle = exports.Obstacle = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _variables = __webpack_require__(0);

var variable = _interopRequireWildcard(_variables);

var _utilities = __webpack_require__(1);

var utility = _interopRequireWildcard(_utilities);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Obstacle = exports.Obstacle = function Obstacle(x, y, dx, cwidth, cheight, hasUpdatedScore) {
  _classCallCheck(this, Obstacle);

  this.x = x;
  this.y = y;
  this.dx = dx;
  this.cwidth = cwidth;
  this.cheight = cheight;
  this.hasUpdatedScore = hasUpdatedScore;
};

/* ------ BIG OBSTACLE CLASS ------------ */


var BigObstacle = exports.BigObstacle = function (_Obstacle) {
  _inherits(BigObstacle, _Obstacle);

  function BigObstacle(x, y, dx, cwidth, cheight, hasUpdatedScore) {
    _classCallCheck(this, BigObstacle);

    return _possibleConstructorReturn(this, (BigObstacle.__proto__ || Object.getPrototypeOf(BigObstacle)).call(this, x, y, dx, cwidth, cheight, hasUpdatedScore));
  }

  _createClass(BigObstacle, [{
    key: "update",
    value: function update() {
      this.dx = -3.5 * variable.velocityfactor;

      this.x += this.dx;

      if (parseInt(this.x + this.cwidth) <= 0 && this.hasUpdatedScore == false) {
        variable.player_score = parseInt(variable.player_score) + parseInt(10);
        this.hasUpdatedScore = true;
      }

      this.draw();
    }
  }, {
    key: "draw",
    value: function draw() {
      variable.ctx.beginPath();
      variable.ctx.drawImage(variable.bigObj, this.x, this.y, 80, 76);
    }
  }]);

  return BigObstacle;
}(Obstacle);

/* ------ SMALL OBSTACLE CLASS ------------ */


var SmallObstacle = exports.SmallObstacle = function (_Obstacle2) {
  _inherits(SmallObstacle, _Obstacle2);

  function SmallObstacle(x, y, dx, cwidth, cheight, hasUpdatedScore) {
    _classCallCheck(this, SmallObstacle);

    return _possibleConstructorReturn(this, (SmallObstacle.__proto__ || Object.getPrototypeOf(SmallObstacle)).call(this, x, y, dx, cwidth, cheight, hasUpdatedScore));
  }

  _createClass(SmallObstacle, [{
    key: "update",
    value: function update() {
      this.dx = -3.5 * variable.velocityfactor;

      this.x += this.dx;

      if (parseInt(this.x + this.cwidth) <= 0 && this.hasUpdatedScore == false) {
        variable.player_score = parseInt(variable.player_score) + parseInt(10);
        this.hasUpdatedScore = true;
      }

      this.draw();
    }
  }, {
    key: "draw",
    value: function draw() {
      variable.ctx.beginPath();
      variable.ctx.drawImage(variable.bigObj, this.x, this.y, 38, 76);
    }
  }]);

  return SmallObstacle;
}(Obstacle);

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Player = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _variables = __webpack_require__(0);

var variable = _interopRequireWildcard(_variables);

var _utilities = __webpack_require__(1);

var utility = _interopRequireWildcard(_utilities);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Player = exports.Player = function () {
  function Player(x, y, dx, dy, playerwidth, playerheight) {
    _classCallCheck(this, Player);

    this.x = x;
    this.y = y;
    this.playerwidth = playerwidth;
    this.playerheight = playerheight;
    this.dx = dx;
    this.dy = dy;
  }

  _createClass(Player, [{
    key: "draw",
    value: function draw() {
      variable.ctx.beginPath();
      variable.ctx.drawImage(variable.imageObj, this.playerwidth * variable.currentframe, 0, this.playerwidth, this.playerheight, this.x, this.y, 108, 140);
    }
  }, {
    key: "hasCollided",
    value: function hasCollided(obstacles) {
      for (var j = 0; j < obstacles.length; j++) {
        if (this.x + this.playerwidth - 20 >= obstacles[j].x && obstacles[j].x + obstacles[j].cwidth >= this.x && this.y + this.playerheight >= obstacles[j].y && obstacles[j].y + obstacles[j].cheight >= this.y) {
          console.log('COLLISION DUDE!!!!!!!!');
          cancelAnimationFrame(variable.gameanimation);
          document.querySelector('.gameoverscreen span').innerText = variable.player_score;
          document.querySelector('.gameoverscreen').classList.add('open');
        };
      }
    }
  }, {
    key: "update",
    value: function update(obstacles) {
      // if up was pressed and its not already jumping, start decreasing its y velocity.
      if (variable.up && variable.jumping == false) {
        console.log('jumping');
        this.dy -= variable.jumpingamount;
        variable.jumping = true;
        variable.currentframe = 0;
      }

      this.dy += variable.gravity; // gravity
      this.y += this.dy;
      this.dy *= variable.friction; // friction

      // if rectangle goes past the height of canvas stick it there (freeze asshole)!
      if (this.y > variable.canvas.height - this.playerheight - 60) {
        variable.jumping = false;
        this.y = variable.canvas.height - this.playerheight - 60;
        this.dy = 0;
      }

      this.draw();

      // ======================================================
      //detecting collision with obstacles!!!!!!!!!!!!!!!!!!!!!
      // ======================================================
      this.hasCollided(obstacles);
    }
  }]);

  return Player;
}();

/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(2);
module.exports = __webpack_require__(3);


/***/ })
/******/ ]);