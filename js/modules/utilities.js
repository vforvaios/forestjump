import * as variable from "../modules/variables.js"

export function randomX(between, to) {
  return  Math.floor(Math.random()*(to-between+1)+between)
}

export function update_player_score() {
  variable.ctx.font = "30px Arial";
  variable.ctx.strokeStyle = 'black';
  variable.ctx.lineWidth = 4;
  variable.ctx.strokeText(variable.player_score,10,50);
  variable.ctx.fillStyle = "white";
  variable.ctx.fillText(variable.player_score,10,50);
}

export function clearCanvas() {
  variable.ctx.clearRect(0, 0, variable.canvas.width, variable.canvas.height);
}

export function keyListener(event) {
  
  var key_state = (event.type == "keydown")?true:false;

  switch(event.keyCode) {

    case 38:// up key
      variable.up = key_state;
      console.log(variable.up);
      break;
    case 32:// space bar
      variable.shooting = key_state
      if(variable.shooting) {
        variable.fireburst = new Audio();
        if(variable.fireburst.canPlayType('audio/mp3')) {
          variable.fireburst.src = 'audio/laser.mp3';
        } else {
          variable.fireburst.src = 'audio/laser.wav'
        }
        variable.fireburst.play();
      }

  }

}

export function removeObstacles() {
  var i = variable.obstacle_array.length;

  while (i--) {
    var obstacle = variable.obstacle_array[i];

    if (obstacle.hasUpdatedScore) {
      variable.obstacle_array.splice(i, 1);
    }
  }
}