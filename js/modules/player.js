import * as variable from "../modules/variables.js"
import * as utility from "../modules/utilities.js"

export class Player {
  constructor(x, y, dx, dy, playerwidth, playerheight) {
    this.x = x;
    this.y = y;
    this.playerwidth = playerwidth;
    this.playerheight = playerheight;
    this.dx = dx;
    this.dy = dy;
  }

  draw() {
    variable.ctx.beginPath();
    variable.ctx.drawImage(variable.imageObj, this.playerwidth*variable.currentframe , 0, this.playerwidth, this.playerheight, this.x, this.y, 108, 140);
  }

  hasCollided(obstacles) {
    for (var j=0; j<obstacles.length; j++) {
      if ((this.x + this.playerwidth - 20 >= obstacles[j].x) &&
        (obstacles[j].x + obstacles[j].cwidth >= this.x) &&
        (this.y + this.playerheight >= obstacles[j].y) && 
        (obstacles[j].y + obstacles[j].cheight >= this.y)) {
        console.log('COLLISION DUDE!!!!!!!!');
        cancelAnimationFrame(variable.gameanimation);
        document.querySelector('.gameoverscreen span').innerText = variable.player_score;
        document.querySelector('.gameoverscreen').classList.add('open');
      };

    }
  }

  update(obstacles) {
    // if up was pressed and its not already jumping, start decreasing its y velocity.
    if (variable.up && variable.jumping == false) {
      console.log('jumping');
      this.dy -= variable.jumpingamount;
      variable.jumping = true;
      variable.currentframe = 0;
    }
    
    this.dy += variable.gravity;// gravity
    this.y += this.dy;
    this.dy *= variable.friction;// friction

    // if rectangle goes past the height of canvas stick it there (freeze asshole)!
    if (this.y > variable.canvas.height - this.playerheight - 60) {
      variable.jumping = false;
      this.y = variable.canvas.height - this.playerheight - 60;
      this.dy = 0;
    }

    this.draw();


    // ======================================================
    //detecting collision with obstacles!!!!!!!!!!!!!!!!!!!!!
    // ======================================================
    this.hasCollided(obstacles);
    
  }
}