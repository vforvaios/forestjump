import * as variable from "../modules/variables.js"

export class Background {
  constructor(x, y, speed, imagetodraw) {
    this.x = x;
    this.y = y;
    this.speed = speed;
    this.imagetodraw = imagetodraw
  }

  draw() {
    variable.ctx.beginPath();
    variable.ctx.drawImage(this.imagetodraw, this.x, this.y, variable.canvas.width, variable.canvas.height);
  }

  update() {
    this.x -= this.speed;

    if(this.x + variable.canvas.width <= 0) {
      console.log('repeat bg');
      this.x = variable.canvas.width - 2;
    }

    this.draw();
  }
}