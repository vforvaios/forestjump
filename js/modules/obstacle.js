import * as variable from "../modules/variables.js"
import * as utility from "../modules/utilities.js"

export class Obstacle {
  constructor(x, y, dx, cwidth, cheight, hasUpdatedScore) {
    this.x = x;
    this.y = y;
    this.dx = dx;
    this.cwidth = cwidth;
    this.cheight = cheight;
    this.hasUpdatedScore = hasUpdatedScore;
  }
}


/* ------ BIG OBSTACLE CLASS ------------ */
export class BigObstacle extends Obstacle {
  constructor(x, y, dx, cwidth, cheight, hasUpdatedScore) {
    super(x, y, dx, cwidth, cheight, hasUpdatedScore);
  }

  update() {
    this.dx = -3.5*variable.velocityfactor;

    this.x += this.dx;

    if((parseInt(this.x + this.cwidth) <= 0) && this.hasUpdatedScore==false ) {
      variable.player_score = parseInt(variable.player_score) + parseInt(10);
      this.hasUpdatedScore = true;
    }

    this.draw();

  }

  draw() {
    variable.ctx.beginPath();
    variable.ctx.drawImage(variable.bigObj, this.x, this.y, 80, 76);

  }
}

/* ------ SMALL OBSTACLE CLASS ------------ */
export class SmallObstacle extends Obstacle {
  constructor(x, y, dx, cwidth, cheight, hasUpdatedScore) {
    super(x, y, dx, cwidth, cheight, hasUpdatedScore);
  }

  update() {
    this.dx = -3.5*variable.velocityfactor;

    this.x += this.dx;

    if((parseInt(this.x + this.cwidth) <= 0) && this.hasUpdatedScore==false ) {
      variable.player_score = parseInt(variable.player_score) + parseInt(10);
      this.hasUpdatedScore = true;
    }

    this.draw();

  }

  draw() {
    variable.ctx.beginPath();
    variable.ctx.drawImage(variable.bigObj, this.x, this.y, 38, 76);

  }
}