import * as variable from "../modules/variables.js"

export class Bullet {
  constructor(x, y, dx, bulletwidth, bulletheight) {
    this.x = x;
    this.y = y;
    this.dx = dx;
    this.bulletwidth = bulletwidth;
    this.bulletheight = bulletheight;
  }

  update(player) {
    if (variable.shooting == true) {
      variable.shooting = false;
    }
    
    this.x += this.dx;

    this.draw();
  };

  draw() {
    variable.ctx.beginPath();
    variable.ctx.fillStyle = "red";
    variable.ctx.fillRect(this.x, this.y, this.bulletwidth, this.bulletheight);
  };
}