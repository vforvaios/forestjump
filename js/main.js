import * as variable from "./modules/variables.js"
import * as utility from "./modules/utilities.js"
import {Obstacle, BigObstacle, SmallObstacle} from "./modules/obstacle.js"
import {Player} from "./modules/player.js"
import {Bullet} from "./modules/bullet.js"
import {Background} from "./modules/background.js"

var Game = {
  start() {
    utility.clearCanvas();
    let splashScreen = document.querySelector('.splashscreen');
    let gameoverscreen = document.querySelector('.gameoverscreen');

    variable.canvas.classList.add('playing');
    splashScreen.classList.remove('open');
    splashScreen.classList.add('not-displayed');
    gameoverscreen.classList.remove('open');

    init();
    animate();
  },
  stop() {

  }
}

// NOT YET USED!!!!!!!!!!!!!!!!!!!!!!!!!
function returnCollision(x1, y1, w1, h1, x2, y2, w2, h2) {
  if ((x1 + w1 >= x2) &&
      (x2 + w2 >= x) &&
      (y1 + h1 >= y2) && 
      (y2 + h2 >= y1)) {
    return true;
  } else {
    return false;
  }
}

/* ========================== */
var bg1, bg2, bg3, bg4;

function initBackground() {
  variable.canvas_bg = new Image();
  variable.ground_bg = new Image();

  variable.canvas_bg.onload = function() {
    variable.ctx.drawImage(variable.canvas_bg, 0, 0);
  }
  variable.ground_bg.onload = function() {
    variable.ctx.drawImage(variable.ground_bg, 0, 300);
  }

  variable.canvas_bg.src = 'images/forest.png';
  variable.ground_bg.src = 'images/ground.png';
  
  bg1 = new Background(0, 0, variable.bgspeed, variable.canvas_bg);
  bg2 = new Background(variable.canvas.width, 0, variable.bgspeed, variable.canvas_bg);

  bg3 = new Background(0, 190, variable.groundspeed, variable.ground_bg);
  bg4 = new Background(variable.canvas.width, 190, variable.groundspeed, variable.ground_bg);
}

function initObstacleImages() {
  variable.bigObj = new Image();
	variable.smallObj = new Image();

  variable.bigObj.src = 'images/spike-1.png'; 
  variable.smallObj.src = 'images/spike-2.png';	
}

function initPlayer() {

  variable.imageObj = new Image();

  variable.imageObj.onload = function() {
    variable.ctx.drawImage(variable.imageObj, 0, 0);
  };
  variable.imageObj.src = 'images/player3.png';
}

function init() {
  resizeCanvas();
  
  initPlayer();
  initObstacleImages();
  initBackground();

  variable.player = new Player(30, variable.canvas.height/2, 0, 0, variable._playerwidth, variable._playerheight);

  //get current timestamp in seconds
  variable.nowstamp = Math.floor(Date.now() / 1000);
  variable.playerframenow = Math.floor(Date.now());

}

function resizeCanvas() {
  let canvasWidth = variable.canvas.width;
  let canvasHeight = variable.canvas.height;
  variable.desiredWidth = window.innerWidth;
  let ratio = canvasWidth / canvasHeight;

  variable.desiredHeight = variable.desiredWidth / ratio;

  variable.canvas.style.width = variable.desiredWidth + "px";
  variable.canvas.style.height = variable.desiredHeight + "px";

  initBackground();
}




function animate() {
  variable.gameanimation = requestAnimationFrame(animate);
  
  utility.clearCanvas();

  bg1.update();
  bg2.update();
  bg3.update();
  bg4.update();

  variable.playerframedesired = Math.floor(Date.now());
  
  if(!variable.jumping) {

    if (variable.currentframe >= variable.playerframes) {
      variable.currentframe = 0;
    } else {
      if(variable.playerframedesired-variable.playerframenow >= variable.playerframestamp) {
        variable.currentframe += 1;
        variable.playerframenow = Math.floor(Date.now());

      }
    }
  } else {
    variable.currentframe = 0;
  }

  variable.player.update(variable.obstacle_array);

  variable.desiredstamp = Math.floor(Date.now() / 1000);

  // when more seconds have passed than deltastamp, spawn a new obstacle and push it into the array of obstacles
  if(variable.desiredstamp-variable.nowstamp >= variable.deltastamp ) {
    if(utility.randomX(1, 2) == 1) {
      variable.obstacle = new BigObstacle(utility.randomX(variable.canvas.width + 100, variable.canvas.width + 150), variable.canvas.height-150, 4, 80, 76, false);
    } else {
      variable.obstacle = new SmallObstacle(utility.randomX(variable.canvas.width + 100, variable.canvas.width + 150), variable.canvas.height-150, 4, 38, 76, false);
    }
    variable.obstacle_array.push(variable.obstacle);
    variable.nowstamp = Math.floor(Date.now() / 1000);
  }

  // render the obstacles
  for(var i=0;i<variable.obstacle_array.length;i++) {
    variable.obstacle_array[i].update();
  }

  // checking if spacebar is hit, and create a bullet and store it.
  if(variable.shooting) {
    variable.bullet = new Bullet(variable.player.x+variable._playerwidth, variable.player.y+ variable._playerheight/2, 10, 6, 2);
    variable.bullets.push(variable.bullet);
  }

  // iterate bullets and draw them
  for(var k=0; k<variable.bullets.length; k++) {
    variable.bullets[k].update(variable.player);
  }

  // remove obstacles when they have updated score (and they are offscreen)
  utility.removeObstacles();
  utility.update_player_score();

  
}

/* ============= */
window.onload = function() {
  let startGame = document.querySelector('.startGame');
  let restartGame = document.querySelector('.restartgame');
  startGame.addEventListener('click', Game.start);
  //restartGame.addEventListener('click', Game.start);

  window.addEventListener('keydown', utility.keyListener);
  window.addEventListener('keyup', utility.keyListener);
  setInterval(function() {
    variable.velocityfactor += .3;
  }, 10000)
  //Game.start();
}

window.addEventListener('resize', resizeCanvas);